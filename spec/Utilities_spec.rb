require 'spec_helper'

describe Utilities do

  #
  class UtilitiesFake
    include Utilities
  end

  describe '.division' do
    subject(:division) do
      UtilitiesFake.new.division(dividend, divisor)
    end

    context 'correct division' do
      let (:dividend) { 10 }
      let (:divisor)  { 5 }
      it 'returns a correct division' do
        expect(division).to eq(2)
      end
    end

    context 'correct division with zero' do
      let (:dividend) { 10 }
      let (:divisor)  { 0 }
      it 'returns zero' do
        expect(division).to eq(0)
      end
    end
  end

  describe '.extractNumber' do
    subject(:extractNumber) do
      UtilitiesFake.new.extractNumber(string)
    end

    context 'string with number in the end' do
      let (:string) { 'qwerty1' }
      it 'returns a number' do
        expect(extractNumber).to eq('1')
      end
    end

    context 'string without number' do
      let (:string) { 'asd' }
      it 'returns a empty string' do
        expect(extractNumber).to eq('')
      end
    end
  end
end
