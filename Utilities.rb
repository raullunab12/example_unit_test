require 'spec_helper'
#
module Utilities

    def division(dividend, divisor)
        return 0 if divisor == 0
        dividend / divisor
    end

    def extractNumber(string)
        string.each_char { |letter| return letter if letter =~ /[0-9]/ }
        ''
    end
end